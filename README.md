Vagrant Template for Wordpress
---

This is a very minimal Vagrant configuration to work with Wordpress.

All you have to do is clone this repo (or simply copy the Vagrantfile) and run the command `vagrant up`. That's all it takes.

**Quick Info:**

- Intended Vagrant Provider: Virtualbox
- Vagrant Box: Ubuntu Bionic 64-bit
- Database MariaDB (drop-in replacement for MySQL)
- Apache2
- PHP 7.2 (Including libraries for mysql, gd (for image manipulation), and curl)

## Overview

**Here is what the directory structure looks like initially**:

```
├── README.md
├── .gitignore
└── Vagrantfile
```

**Here is what the directory structure looks like after running `vagrant up`**:

```
├── media/        *
├── plugins/      *
├── themes/       *
├── .gitignore
├── README.md
├── Vagrantfile
└── wp-config.php *
```

*New files/folders indicated with `*`.* 

## Usage

Clone this repository, cd into it, and run `vagrant up`. You can accomplish that with the following commands:

```bash
git clone https://gitlab.com/dougbeney/vagrant-wordpress.git YOUR_PROJECT_NAME
cd YOUR_PROJECT_NAME
vagrant up
```

Visit [localhost:8080](http://localhost:8080) in your browser to view the site.

## Gitignore

You can modify the `.gitignore` file to ignore/not-ignore the `plugins/`, `themes/`, and `media/` folders.

## Database import/export

You can import/export a database by using the following commands.

### Import

```
vagrant ssh -c "sudo mysql -u root -p='' wordpress < /vagrant/myfile.sql"
```

### Export

```
vagrant ssh -c "sudo mysqldump -u root -p="" wordpress > /vagrant/backup.sql"
```
