# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "ubuntu/bionic64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  #config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
  config.vm.provision "shell", inline: <<-SHELL
    apt-get update
    apt-get install apache2 mariadb-server mariadb-client -y
    apt-get install php php-mysql php-curl php-gd php-xml -y
    apt-get install unzip -y

# If Wordpress does not exist (checking with a wp-specific file) install it
  if [ ! -f /var/www/html/wp-login.php ]; then
    mv /var/www/html /var/www/apache-default
    mkdir /var/www/html
    wget -P /var/www/html/ https://wordpress.org/latest.zip
    unzip /var/www/html/latest.zip -d /var/www/html
    mv /var/www/html/wordpress/* /var/www/html/
    rm /var/www/html/latest.zip
  fi

    mysql -u root -p="" -e "create database if not exists wordpress"
    mysql -u root -p="" -e "GRANT ALL PRIVILEGES ON *.* TO 'wordpress'@'localhost' IDENTIFIED BY 'wordpress'"

    if [ -d "/vagrant/themes"  ]; then
      rm -rf /var/www/html/wp-content/themes
    else
      mv /var/www/html/wp-content/themes /vagrant/themes
    fi
    if [ -d "/vagrant/plugins"  ]; then
      rm -rf /var/www/html/wp-content/plugins
    else
      mv /var/www/html/wp-content/plugins /vagrant/plugins
    fi

    mkdir -p /vagrant/media /vagrant/plugins /vagrant/themes
    ln -sf /vagrant/media /var/www/html/wp-content/media
    ln -sf /vagrant/plugins /var/www/html/wp-content/plugins
    ln -sf /vagrant/themes /var/www/html/wp-content/themes

# Allow for permalinks
# Annoying indentation because of EOF
cat << EOF > /etc/apache2/sites-enabled/000-default.conf
<VirtualHost *:80>
  ServerAdmin webmaster@localhost
  DocumentRoot /var/www/html
  ErrorLog ${APACHE_LOG_DIR}/error.log
  CustomLog ${APACHE_LOG_DIR}/access.log combined
  <directory /var/www/html>
    AllowOverride All
  </directory>
</VirtualHost>
EOF

# wp-config
# Annoying indentation because of EOF
if [ ! -f /vagrant/wp-config.php ]; then
cat << EOF > /vagrant/wp-config.php
<?php
define('WP_DEBUG', true);

define('DB_NAME', 'wordpress');
define('DB_USER', 'wordpress');
define('DB_PASSWORD', 'wordpress');
define('DB_HOST', 'localhost');
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');
\\$table_prefix  = 'wp_';

if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

require_once(ABSPATH . 'wp-settings.php');
EOF
ln -sf /vagrant/wp-config.php /var/www/html/wp-config.php
else
ln -sf /vagrant/wp-config.php /var/www/html/wp-config.php
fi

    sed -ie "s/www-data/vagrant/g" /etc/apache2/envvars
    chown vagrant:vagrant -R /var/www
    a2enmod rewrite
    systemctl enable apache2
    systemctl restart apache2
    echo "The site is running at http://localhost:8080"
  SHELL
end
